<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>welkom op timkwaijtaal.nl</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Welkom op timkwaijtaal.nl
                </div>

                <div>
                <p>Welkom mensen op mijn portfolio-website. Ik ben Tim Kwaijtaal en ben 25 jaar oud, woon sinds kort in het mooie Utrecht zelfstandig. In mijn vrije tijd houd ik mijn voornamelijk bezig met muziek produceren, programmeren, rugby, hardlopen, wandelen, fietsen, enz. Ik heb veel passie voor muziek en innovaties, omdat ik daar verder in verdiep en daar mijn eigen creatieve merk wil uitbrengen.</p>
                </div>

                <div class="links">
                    <a href="https://www.linkedin.com/in/tim-kwaijtaal-10a66aa3/">Mijn-Linkedin</a>
                    <a href="https://gitlab.com/timkwaijtaal">Mijn-Gitlab</a>
                </div>
            </div>
        </div>
    </body>
</html>
